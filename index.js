import dotenv from "dotenv"
import express, { application } from "express"
import cors from 'cors'
import registerRoutes from "./src/routes/register.js"
import loginRoutes from "./src/routes/login.js"
import profileRoutes from "./src/routes/profile.js"
import bucketRoutes from "./src/routes/bucket.js"
import tripRoutes from "./src/routes/trip.js"
import featuredRoutes from "./src/routes/featured.js"
import createAdmin from "./src/routes/createAdmin.js"
import deleteAdmin from "./src/routes/deleteAdmin.js"
import getAdmin from "./src/routes/getAdmin.js"
import updateAdmin from "./src/routes/updateAdmins.js"
import contactRoutes from "./src/routes/contact.js"

dotenv.config()

const app = express()
const port = process.env.PORT || 5000

app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())


app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.use('/bucket', bucketRoutes)
app.use('/register',registerRoutes)
app.use('/auth',loginRoutes)
app.use('/profile',profileRoutes)
app.use('/trip',tripRoutes)
app.use('/featured',featuredRoutes)
app.use('/admin/create',createAdmin)
app.use('/admin/delete',deleteAdmin)
app.use('/admin/get',getAdmin)
app.use('/admin/update',updateAdmin)
app.use('/contact', contactRoutes)




app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})