# BucketList Application Backend

1. Link to Front End Web Application - https://gitlab.com/Jonathan.Francis/frontend_bucketlist_vacation
2. Link to Back End Web Application - https://gitlab.com/Jonathan.Francis/backend_nodejs_bucketlist_vacation


# Information about the .env file

1. This project uses the .env file to store important information for this application
2. Rename the .envExample to .env and adjust the properties in this file accordingly
3. PORT refers to the port this application will use on your system
4. JWT_Secret will sign and issue tokens for this application and can be used later for verification
5. It is important to note that the Docker & npm use different .env instances so be sure to select the appropriate one when using either Docker or npm.

# Using Docker to run this project

1. You can run this project using docker compose
2. Clone the repo using SSH or HTTP
3. Ensure that Docker is installed
4. Update the JWT_SECRET variable in the Dockerfile if you wish to run this locally using docker compose
5. Use the following command to start this application:

a) docker-compose build 
b) docker-compose up

6. This should start both the API and the MySQL server required for this project
7. Connect to the SQL server and run the BucketList.sql file in your preferred SQL administration software.(Import the BucketList.sql file into MySQL Workbench).

# Run this project using npm

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:5000 to run so ensure that port 5000 is not in use (you can change the port by going into .env and adjusting the variable of port from 5000 to something else)
5. In the terminal you can type in npm run start to start this project
6. Connect to the SQL server and run the BucketList.sql file in your preferred SQL administration software.(Import the BucketList.sql file into MySQL Workbench).

# Information on the BucketList.sql file and database

1. Create a user in your database that matches the information in the .envexample file. You can do this using the following SQL statement:

a) CREATE USER 'nodeuser'@'localhost' IDENTIFIED BY 'buck3tl1st';

b) GRANT ALL PRIVILEGES ON * . * TO 'nodeuser'@'localhost';

c) FLUSH PRIVILEGES;

d) Port used in MySQL Workbench database when using npm will be 3306 and for Docker it will be 9003.

2. To ensure that the frontend works correctly, please import the BucketList.sql file in your preferred SQL administration software (MySQL Workbench, WinSQL etc.)

a) Once the data from BucketList.sql has been imported into MySQL Workbench, you will need to enter in the following commmand to access the database, (Name of database bucketlist), and its tables in MySQL Workbench. See below.

USE bucketlist;


3. If you have imported the - BucketList.sql file -  already in step 2 into your MySQL Workbench , you will have already created an admin user for the website to manage the users: If for some reason it was not imported or missed here are the values to add, see below:

INSERT INTO `bucketlist`.`users` (`userid`, `username`, `email`, `password`, `phoneNumber`, `fullName`, `admin`) VALUES ('833cecaf-4806-4f19-80b1-b2f59a1044af', 'admin', 'admin@admin.com', '$2b$10$Aw.lyEUj0HaYZA2ITleanOztdSzeiENmt2.SnmEtnvVsUYzXzLT1q', '5551231234', 'Administrator', '1');

To login use the following:

Email: admin@admin.com
Password: adminpassword

4. To test if your connection is successful, type the following in your terminal:

node .\util\connection.js

This should say connected with an id. 


# Ready to Go

After the steps above have been completed, you should be ready to go and you can run the backend using the following in your terminal:

npm start
OR
npm run dev
