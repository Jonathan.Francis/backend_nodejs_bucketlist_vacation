import db from "../util/connection.js "
import { v4 as uuidv4 } from 'uuid';


const addAdmin = (data) => {
  const { fullName, username, email, password, phoneNumber, admin } = data
    return new Promise ((resolve, reject) => {
      db.query(
      `INSERT INTO users (userid, fullName, username, email, password, phoneNumber, admin ) VALUE (?, ?, ?, ?, ?, ?, ?)`, 
      [
        uuidv4(),
        fullName,
        username,
        email,
        password,
        phoneNumber,
        admin
      ],
      (error, results) => {
        if (error){
          console.log(error)
          return reject(error)
        } else {
          return resolve(results)
        }
      }
    )
  })
}

export default addAdmin

