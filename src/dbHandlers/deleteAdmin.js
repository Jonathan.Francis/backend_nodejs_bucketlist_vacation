import db from "../util/connection.js"

const deleteAdmin = (field, value) => {
  return new Promise((resolve, reject) => {
      db.query(`DELETE FROM users where ${field} IN("${value}")`, 
        (error, results) => {
          if (error){
            return reject(error)
          } else {
            return resolve(results)
          }
        }
      )
    }
  )
}

export default deleteAdmin 