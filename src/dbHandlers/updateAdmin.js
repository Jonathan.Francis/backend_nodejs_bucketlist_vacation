import db from "../util/connection.js"


const updateAdmin = async (data, id) => {
  const { fullName, username, email, password, phoneNumber, admin } = data

  return new Promise ((resolve, reject) => {
    db.query(
      `UPDATE users
      SET username=?, email=?, password=?, phoneNumber=?, fullName=?, admin=?
      WHERE userid = ?;`, 
      [
        username,
        email,
        password,
        phoneNumber,
        fullName,
        admin,
        id
      ],

      (error, results) => {
        if (error){
          return reject(error)
        } else {
          return resolve(results)
        } 
      }
    )
  })
}

export default updateAdmin

      
      