import express from "express"
import db from "../util/connection.js";
import jwtVerify from "../middleware/jwtVerify.js"
import { v4 as uuidv4 } from 'uuid';

const router = express.Router()

/**Get Bucket List for User**/
router.get("/:id", jwtVerify, (req, res) => {
  let id = req.params.id
  db.query(
    `SELECT * FROM bucket WHERE userid=?`, id,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**Add Bucket List Item**/
router.post("/:id", (req, res) => {
  db.query(
    "INSERT INTO bucket (bucketid, name, location, image, budget, category, targetDate, participants, status, userid, tripid) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
    [
      uuidv4(),
      req.body.name, 
      req.body.location,
      req.body.image,
      req.body.budget,
      req.body.category,
      req.body.targetDate,
      req.body.participants,
      req.body.status,
      req.body.userid,
      req.body.tripid,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

/**To Update/Edit Bucket List Item**/
router.put("/:id", (req, res) => {
  db.query(
    `UPDATE bucket SET name = ? , location = ? , image = ?, budget = ?, category = ?, targetDate = ?, participants = ?, status = ? WHERE bucketid = ? `,
    [
      req.body.name, 
      req.body.location,
      req.body.image,
      req.body.budget,
      req.body.category,
      req.body.targetDate,
      req.body.participants,
      req.body.status,
      req.params.id,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**To Update/Edit Bucket List Item**/
router.patch("/:id", (req, res) => {
  db.query(
    `UPDATE bucket SET status = ? WHERE bucketid = ?`,
    [
      req.body.status,
      req.params.id
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**To Delete Bucket List Item**/
router.delete("/:id", (req, res) => {
  let id = req.params.id
  db.query(
    `DELETE FROM bucket WHERE bucketid=?`, id,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;