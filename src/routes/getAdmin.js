import express from "express"
import searchAdmin from "../dbHandlers/searchAdmin.js";
import getAdmin from "../dbHandlers/getAdmin.js";
import getAll from "../dbHandlers/getAll.js";

const router = express.Router()

router.get('/all', async (req, res) => {
  try {
    let admins = await getAll("users")
    return res.status(200).json(admins)
  } catch (err) {
    return res.status(404).send(err)
  }

})

router.post('/search', async (req, res) => {
  try {
    let admins = await searchAdmin(req.body)
    return res.status(200).send(admins)
  } catch (err) {
    return res.status(404).send(err)
  }
})

router.get('/:id', async (req, res) => {
  try {
    const id = req.params.id;
    const admin = await getAdmin("userid", id)
    return res.status(200).json(admin[0])
  } catch (err) {
    return res.status(404).send(err)
  }

})

export default router