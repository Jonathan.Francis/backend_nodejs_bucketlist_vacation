import express from "express"
import updateAdmin from "../dbHandlers/updateAdmin.js"

const router = express.Router()

router.patch('/:id', async (req, res) => {
  try {
    let result = await updateAdmin(req.body, req.params.id)
    return res.status(200).json(result)
  } catch (err) {
    return res.status(404).json(err)
  }
  
}) 

export default router