import express from "express"
import db from "../util/connection.js";
import jwtVerify from "../middleware/jwtVerify.js";
import bcrypt from "bcrypt";
const saltRounds = 10;
const router = express.Router()


///*Admin* - View All Profiles/

router.get("/", jwtVerify, (req, res) => {
  db.query(
    "SELECT * FROM users",
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});



///*Profile Page - Once Successfully Logged in.

router.get("/:id", jwtVerify, (req, res) => {
  let id = req.params.id
  db.query(
    `SELECT * FROM users WHERE userid=?`, id,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json(results);
    }
  );
})


///*Profile Page - EDIT PAGE.
router.put("/:id", jwtVerify, (req, res) => {
  db.query(`UPDATE users SET username=?, email=?, password=?, phoneNumber=?, fullName=?, admin=?, location=?, interests=?, image=? WHERE userid=?`,
    [
      req.body.username,
      req.body.email,
      req.body.password,
      req.body.phoneNumber,
      req.body.fullName,
      req.body.admin,
      req.body.location,
      req.body.interests,
      req.body.image,
      req.params.id
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json(results);
    }
  );
})


router.patch("/:id", jwtVerify, (req, res) => {
  const hash = bcrypt.hashSync(req.body.password, saltRounds);

  db.query(
    `UPDATE users SET username=?, email=?, password=?, phoneNumber=?, fullName=?, admin=?, location=?, interests=?, image=? WHERE userid=?`,
    [
      req.body.username,
      req.body.email,
      hash,
      req.body.phoneNumber,
      req.body.fullName,
      req.body.admin,
      req.body.location,
      req.body.interests,
      req.body.image,
      req.params.id
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).json(results);
    }
  );
})


///*Profile Page - EDIT PAGE - delete profile.
router.delete("/:id", jwtVerify, (req, res) => {
  let id = req.params.id
  db.query(
    `DELETE FROM users WHERE userid=?`, id,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


export default router;