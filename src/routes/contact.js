import express from 'express'
import jwtVerify from "../middleware/jwtVerify.js"
import db from "../util/connection.js"
const router = express.Router()

// Route to display all entries (Required Verification)
router.get('/entries', jwtVerify, (req, res) => {
    db.query('SELECT * FROM contact', function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

// Route to display one entry by ID (Required Verification)
router.get('/entries/:id', jwtVerify, (req, res) => {
    const id = req.params.id

    db.query('SELECT * FROM contact WHERE contactid = ?', id, function(err,result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else if (result.length > 0) {
            return res.status(200).json(result)
        } else {
            return res.status(404).json({message: `${id} not found`})
        }
    })
})

// Route to post an entry
router.post('/entries', jwtVerify, (req, res) => {
    let newEntry = req.body

    db.query('INSERT INTO contact SET ?', newEntry, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(newEntry)
        }
    })
})

// Route to delete a contact entry
router.delete('/entries/delete/:id',jwtVerify, (req, res) => {
    let id = req.params.id

    db.query('DELETE FROM contact WHERE contactid=?', id, function(err, result) {
        if (err) {
            console.log(err)
            return res.status(500).json({message: 'unable to retrieve data'})
        } else {
            return res.status(200).json(result)
        }
    })
})

export default router
