import express from "express"
import db from "../util/connection.js";

const router = express.Router()

/**Get Featured Trips**/
router.get("/", (req, res) => {
    db.query(
        "SELECT * FROM trip WHERE featured=1",
        function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

export default router;