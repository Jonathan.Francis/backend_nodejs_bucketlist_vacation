import express from "express"
import { v4 as uuidv4 } from 'uuid';
import validateRegister from "../middleware/validateRegister.js";
import db from "../util/connection.js";
import { body, validationResult } from 'express-validator';
import bcrypt from "bcrypt";
const router = express.Router()
const saltRounds = 10;


//*Registeration Page - Create an Account.

router.post("/", validateRegister, body('fullName').notEmpty(), body('email').isEmail(), body('phoneNumber').isMobilePhone(), body('username').notEmpty(), body('password').isLength({ min: 8 }), (req, res) => {

  let registerUser = {
    id: uuidv4(),
    fullName: req.body.fullName,
    email: req.body.email,
    phoneNumber: req.body.phoneNumber,
    username: req.body.username,
    password: req.body.password
  }


  const errors = validationResult(req);

  if (!errors.isEmpty()) {
    const missingValues = [];
    errors.array().forEach(a => {
      missingValues.push(a.param)
    })
    return res.status(400).json({
      message: 'validation error',
      invalid: [`${missingValues}`],
    });
  }

  bcrypt.hash(registerUser.password, saltRounds, function (err, hash) {
    registerUser.password = hash;

    db.query(
      "INSERT INTO users (userid,fullName,email,phoneNumber,username, password) VALUES (?, ?, ?, ?, ?, ?)",
      [registerUser.id, registerUser.fullName, registerUser.email, registerUser.phoneNumber, registerUser.username, registerUser.password],
      function (error, results, fields) {
        if (error) {
          console.log(error)
          if (error.code = 'ER_DUP_ENTRY') { return res.status(400).json({ message: 'e-mail address already exists' }) }
          return res.status(500).json({ message: 'unable to insert data' })
        }
        return res.status(201).json({ message: "Successful Entry" });
      }
    );
  })
})


export default router;

