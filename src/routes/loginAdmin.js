import express from "express"
import jwt from "jsonwebtoken"
import validateLogin from "../middleware/validateLogin.js"
import getAdmin from "../dbHandlers/getAdmin.js"

const router = express.Router()

router.post('/', validateLogin, async (req, res) => {

  try {
    const email = req.body.email;
    const token = jwt.sign({email}, process.env.JWT_SECRET, {expiresIn: '60m'})
    const admin = await getAdmin("email", email)
    return res.status(200).json({token, adminid:admin[0].adminid, fullname:admin[0].fullname, username:admin[0].username, email:admin[0].email, isAdmin:admin[0].isAdmin })
  } catch (err) {
      console.log(err)
      return next(err)
  }
})

export default router



