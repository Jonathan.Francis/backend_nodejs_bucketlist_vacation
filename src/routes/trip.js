import express from "express"
import db from "../util/connection.js";

const router = express.Router()

/**Get Trip List for User**/
router.get("/", (req, res) => {
    db.query(
        "SELECT * FROM trip",
        function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

/**Add Trip Item**/
router.post("/addtrip", (req, res) => {
  db.query(
    "INSERT INTO trip (tripid, name, description) VALUES (?, ?, ?)",
    [
        req.body.tripid,
        req.body.name, 
        req.body.description, 
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(201).send(results);
    }
  );
});

/**Get Bucket List added to Trip**/
router.get("/addbucket/:tripid", (req, res) => {
    let id = req.params.tripid
    db.query(
        "SELECT * FROM bucket WHERE tripid=?", id,
        function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
    }
  );
});

/**Get Trip Information**/
router.get("/:tripid", (req, res) => {
  let id = req.params.tripid
  db.query(
      "SELECT * FROM trip WHERE tripid=?", id,
      function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
  }
);
});

/**To Update/Edit Trip**/
router.put("/:tripid", (req, res) => {
  db.query(
    `UPDATE trip SET name=?, description=?, featured=? WHERE tripid=?`,
    [
      req.body.name, 
      req.body.description,
      req.body.featured,
      req.params.tripid,
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

/**To Delete Trip List Item**/
router.delete("/:tripid", (req, res) => {
  db.query(
    `DELETE FROM trip WHERE tripid=?`,
    [
      req.params.tripid
    ],
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});

export default router;