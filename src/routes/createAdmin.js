import express from "express"
import bcrypt from "bcrypt"
import validateAdmin  from "../middleware/validateAdmin.js"
import addAdmin from "../dbHandlers/addAdmin.js"

const router = express.Router()

  let createHash = async (password) => {
    try {
      const hash = await bcrypt.hash(password, 10)
      return hash
    } catch (err) {
      return res.status(400).send(err)
    }
  }

router.post('/', validateAdmin, async (req, res) => {

  try {
    let newAdmin = await createHash(req.body.password).then(hash => {
      return {...req.body, password: hash}
    })
    await addAdmin(newAdmin)
    return res.status(201).json(newAdmin)
  } catch (err) {
    return res.status(400).send(err)
  }
  
}) 

export default router