import express from "express"
import db from "../util/connection.js"
import jwt from "jsonwebtoken"
import bcrypt from "bcrypt";
import jwtVerify from "../middleware/jwtVerify.js"
const privateKey = "shhhhh"
const router = express.Router()


///*LOGIN PAGE - Registered User gets Token 
router.post('/', (req, res) => {

  const email = req.body.email
  const password = req.body.password

  if (email && password) {
    db.query('SELECT * FROM users WHERE email = ?', email, function (error, results, fields) {
      if (error) {
        console.log(error)
        return res.status(500).json({ message: 'unable to retrieve data' })
      } else if (results.length > 0) {
        let hashedPassword = results[0].password
        let admin = results[0].admin
        const match = bcrypt.compareSync(password, hashedPassword)
        if (match) {
          const token = jwt.sign({email,admin}, privateKey)
          return res.status(201).json(token);
        } else {
          return res.status(401).json({ message: 'incorrect credentials provided' })
        }
      }
      return res.status(401).json({ message: 'incorrect credentials provided' })
    });
  } else {
    return res.status(401).json({ message: 'incorrect credentials provided' })
  }
});

// Get user id from database
router.get('/user/:id', jwtVerify, async (req, res, next) => {
  const {id} = req.params;
  try {
      db.query(
          `SELECT * FROM users WHERE email = ?`, id, 
          function (error, results, fields){
              if (error) throw error;
              const user = results[0];
              if (!user) {
                  return res.status(404).json({message: `user not found`});
              };
              return res.json(user);
          }
      );       
  } catch (err) {
      console.error(err);
      return next(err);
  };
});



export default router