import bcrypt from 'bcrypt'
import { isValidObject } from './validateObject.js'
import getAll from '../dbHandlers/getAll.js'

const verify = async (password, hash) => {
  try {
    const match = await bcrypt.compare(password, hash)
    return match
  } catch (err) {
    throw err
  }
}

const findAdmin = async (username, password) => {
  let content = await getAll("users")
  const userLocation = content.findIndex(user => user.email === username)

  try {
    if(userLocation != -1) {
      if(await verify(password, content[userLocation].password).then(valid => valid)){
        return true
      }
    }

    return false
    } catch (err) {
      console.log(err)
    }
}

const validateLogin = async (req, res, next) => {
    const requiredProperties = ["password", "email"];
    const validObject = isValidObject(req.body, requiredProperties);

    if(validObject){
      try {
        if(await findAdmin(req.body.email, req.body.password)){
          next();
        }
        else {
          return res.status(401).json({message: "incorrect credentials provided"});
        }
        } catch (err) {
        return next(err)
      }
    } 
  
  if(!validObject){
  return res.status(401).json({message: "incorrect credentials provided"});
  }
}

export default validateLogin