import { isValidObject, populateInvalidProperties } from './validateObject.js'

const validateAdmin = (req, res, next) => {
  const requiredProperties = ["fullName", "username", "email", "password", "phoneNumber", "admin"];
  const validObject = isValidObject(req.body = JSON.parse(JSON.stringify(req.body)), requiredProperties);

  if (validObject){
    const validPassword = req.body.password.length >= 8
    const validEmail = /^[^\s@]+@[^\s@]+$/.test(req.body.email);

    if (!validPassword){
      return res.status(400).json({message: "validation error", invalid: "password"});
    }
    if (!validEmail){
      return res.status(400).json({message: "validation error", invalid: "email"});
    }
    else {
      next();
    }
  } 
    else {
      let invalidProperties = populateInvalidProperties(req.body, requiredProperties);
      return res.status(400).json({message: "validation error", invalid: invalidProperties});
    } 
}

export default validateAdmin





