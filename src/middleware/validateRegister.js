//MiddleWare to Validate User Registration

let validateRegister = (req,res, next) => {
    let errorsRegister = []
    let requiredProperties = ["fullName","email","phoneNumber", "username", "password" ]
         requiredProperties.forEach(property => {
            req.body = JSON.parse(JSON.stringify(req.body));
            if(!req.body.hasOwnProperty(property)) {
                errorsRegister.push(property)
        }
     })
        if (errorsRegister.length === 0){
            next();
    }
         if (errorsRegister.length>0) {
            return res.status(400).send({
                message: "validation error",
                invalid: [`${errorsRegister}`]
            }
         )
    }
}

export default validateRegister;