import jwt from 'jsonwebtoken'
let privateKey="shhhhh"

export default (req, res, next) => {
    const authHeader = req.headers['authorization']
    // Value of the authorization header is typically: "Bearer JWT", hence splitting with empty space and getting second part of the split
    const token = authHeader && authHeader.split(' ')[1]
    if (token == null) {
        return res.status(403).json({message: "token not provided"})
    }
    try {
        const data = jwt.verify(token, privateKey)
        req.user = data
        next()
    } catch (err) {
        console.error(err)
        let errmsg = ''
        if (err.message === 'jwt expired') {
            errmsg = 'token expired'
        } else {
            errmsg = 'token not provided'
        }
        return res.status(403).json({message: errmsg})
    }
}
